import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {SubjectDTO, SubjectDTOs, Subjects} from '../../types/subject.type';
import {BehaviorSubject, Observable} from 'rxjs';
import {Subject, SubjectMapper} from '../../models/subject.model';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  private _subjects$: BehaviorSubject<Subjects> = new BehaviorSubject<Subjects>([]);
  get subjects$(): Observable<Subjects> { return this._subjects$.asObservable(); }

  private _editSubject$ = new BehaviorSubject<Subject>(null);
  get editSubject$(): Observable<Subject> { return this._editSubject$.asObservable(); }

  constructor(private $http: HttpClient) {}

  ngOnInit() {
    this.$http
      .get<SubjectDTOs>(`${environment.api_url}/subjects?_expand=user`)
      .pipe(map(a => a.map(it => SubjectMapper.fromDTO(it))))
      .subscribe(data => this._subjects$.next(data));
  }

  handleNewSubject(newItem: Subject) {
    const subjects = this._subjects$.value;
    this._subjects$.next([...subjects, newItem]);
  }

  handleEdit(subject: Subject) {
    this._editSubject$.next(subject);
  }

  handleRemove($event: Subject) {
    const subjects = this._subjects$.value;
    const index = subjects.findIndex(it => it.id == $event.id);

    subjects.splice(index, 1);

    this._subjects$.next(subjects);
  }
}
