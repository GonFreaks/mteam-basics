import {UserDTO} from './user.type';
import {Subject} from '../models/subject.model';

export type Subjects = Array<Subject>;

export type SubjectDTOs = Array<SubjectDTO>;
export type SubjectDTOSet = Set<SubjectDTO>;
export type SubjectDTOMap = Map<number, SubjectDTO>;
export type SubjectDTO = {
  id: number,
  title: string,
  userId: number,
  createAt: string,
  updateAt: string,
  isActive: boolean,
  user?: UserDTO,
  keyWords: Array<string>
}
