import {SubjectDTO} from '../types/subject.type';

export class Subject {
  private _id?: number;
  get id(): number { return this._id; }
  set id(value: number) { this._id = value; }

  private _title: string;
  get title(): string { return this._title; }
  set title(value: string) { this._title = value; }

  private _userId: number;
  get userId(): number { return this._userId; }
  set userId(value: number) { this._userId = value; }

  private _createAt: string;
  get createAt(): string { return this._createAt; }
  set createAt(value: string) { this._createAt = value; }

  private _updateAt: string;
  get updateAt(): string { return this._updateAt; }
  set updateAt(value: string) { this._updateAt = value; }

  private _isActive: boolean;
  get isActive(): boolean { return this._isActive; }
  set isActive(v: boolean) { this._isActive = v; }

  private _user?: any;
  get user(): any { return this._user; }
  set user(value: any) { this._user = value; }

  private _keyWords: Array<string> = [];
  get keyWords(): Array<string> { return this._keyWords; }
  set keyWords(v: Array<string>) { this._keyWords = v; }

  constructor(obj?: Partial<Subject>) {
    this.id = obj && obj.id || null;
    this.title = obj && obj.title || null;
    this.userId = obj && obj.userId || null;
    this.createAt = obj && obj.createAt || null;
    this.updateAt = obj && obj.updateAt || null;
    this.user = obj && obj.user || null;
    this.isActive = obj && obj.isActive || false;
    this.keyWords = obj && obj.keyWords || [];
  }
}

export class SubjectMapper {
  static toDTO(from: Subject): SubjectDTO {
    const {id, title, userId, createAt, updateAt, isActive, keyWords} = from;

    return ({id, title, userId, createAt, updateAt, isActive, keyWords}) as SubjectDTO;
  }

  static fromDTO(from: SubjectDTO): Subject {
    return new Subject(from);
  }
}
