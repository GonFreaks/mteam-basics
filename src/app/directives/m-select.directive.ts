import {AfterViewInit, Directive, ElementRef, Input} from '@angular/core';
import * as M from 'materialize-css';
import {Observable} from 'rxjs';

@Directive({
  selector: '[mSelect]'
})
export class MSelectDirective implements AfterViewInit {
  @Input('mSelect') data$: Observable<any>

  private _mSelect: M.FormSelect;

  get select(): HTMLSelectElement { return this.elRef.nativeElement; }

  constructor(private elRef: ElementRef<HTMLSelectElement>) { }

  ngAfterViewInit() {
    this.data$.subscribe(data => this._mSelect = M.FormSelect.init(this.select, {}));
  }
}
