import {FormDeclaration} from '../types/form.type';
import {FormArray, FormControl, Validators} from '@angular/forms';

//FormControl => correspond à un <input>

export const F_SubjectCreate: FormDeclaration = {
  title: new FormControl(null, [Validators.required]),
  userId: new FormControl(null, [Validators.required, Validators.min(1)]),
  keyWords: new FormArray([], [Validators.minLength(0)])
}
