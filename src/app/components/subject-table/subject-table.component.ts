import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {SubjectDTO, SubjectDTOs, Subjects} from '../../types/subject.type';
import {Subject} from '../../models/subject.model';

@Component({
  selector: 'app-subject-table',
  templateUrl: './subject-table.component.html',
  styleUrls: ['./subject-table.component.scss']
})
export class SubjectTableComponent implements OnInit {
  //Communication Enfant -> Parent
  @Output('edit') editEvent = new EventEmitter<Subject>();
  @Output('remove') removeEvent = new EventEmitter<Subject>();

  //Communication Parent -> Enfant
  @Input() subjects$: Observable<Subjects>;
  @Input('actions')
  set actionsInput(v: string) {
    this.actions = v.split(',');
  }

  actions: Array<string> = [];

  constructor() { }

  ngOnInit(): void {
  }

  editAction(subject: Subject) {
    this.editEvent.emit(subject);
  }

  removeAction(subject: Subject) {
    this.removeEvent.emit(subject);
  }

  hadAction(action: string) {
    return this.actions.find(it => it == action);
  }
}
