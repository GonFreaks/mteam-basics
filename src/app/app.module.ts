import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './components/app/app.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { SubjectFormComponent } from './components/subject-form/subject-form.component';
import { SubjectTableComponent } from './components/subject-table/subject-table.component';
import { MSelectDirective } from './directives/m-select.directive';
import { MTabsDirective } from './directives/m-tabs.directive';

@NgModule({
  declarations: [
    AppComponent,
    SubjectFormComponent,
    SubjectTableComponent,
    MSelectDirective,
    MTabsDirective
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
