import {AfterViewInit, Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[mTabs]'
})
export class MTabsDirective implements AfterViewInit{
  @Input('options') mOptions: Partial<M.TabsOptions> = { };
  private _mTabs: M.Tabs;

  get tabs(): HTMLDivElement { return this.elRef.nativeElement; }

  constructor(private elRef: ElementRef<HTMLDivElement>) { }

  ngAfterViewInit() {
    this._mTabs = M.Tabs.init(this.tabs, {swipeable: true, ...this.mOptions});
    const mTabs = this._mTabs;
    this._mTabs.el
      .querySelectorAll(".tab")
      .forEach(tab => tab.addEventListener("click", function() { /*mTabs.select(this.id);*/ }))
  }
}
