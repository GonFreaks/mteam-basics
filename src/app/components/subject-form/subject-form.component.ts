import {Component, OnInit, Output, EventEmitter, AfterViewInit, Input, ViewChild, ElementRef} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {SubjectDTO} from '../../types/subject.type';
import * as M from 'materialize-css';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {F_SubjectCreate} from '../../forms/subject.form';
import {Subject, SubjectMapper} from '../../models/subject.model';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-subject-form',
  templateUrl: './subject-form.component.html',
  styleUrls: ['./subject-form.component.scss']
})
export class SubjectFormComponent implements OnInit, AfterViewInit {
  @ViewChild("chips", { static: false}) chips: ElementRef<HTMLDivElement>;
  @ViewChild("select", {static: false}) select: ElementRef<HTMLSelectElement>;

  @Output('newSubject') newSubjectEvent = new EventEmitter<Subject>();

  @Input() editSubject$: Observable<any>;

  users$: Observable<Array<any>>;
  //FormGroup => Formulaire
  fg: FormGroup;
  chipInstance: M.Chips;

  get faKeyWords(): FormArray { return this.fg.get('keyWords') as FormArray; }

  /**
   *
   * @param $http
   * @param {FormBuilder} $formBuilder => permet de construire un formulaire
   */
  constructor(private $http: HttpClient, private $formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.fg = this.$formBuilder.group(F_SubjectCreate);
    this.users$ = this.$http.get<Array<any>>(`${environment.api_url}/users`);
  }

  ngAfterViewInit(): void {
    this.chipInstance = M.Chips.init(this.chips.nativeElement, {placeholder: 'Keywords'});

    // this.users$.subscribe(_ => setTimeout(_ => this.selectInstance = M.FormSelect.init(this.select.nativeElement), 500));
    this.editSubject$.subscribe(editSubject => {
      if (!editSubject) { return; }

      this.fg.patchValue({title: editSubject.title, userId: editSubject.userId});
      editSubject?.keyWords?.forEach(
        data => {
          this.faKeyWords.clear();
          this.faKeyWords.push(new FormControl(data))
          this.chipInstance.addChip({tag: data});
          // this.selectInstance = M.FormSelect.init(this.select.nativeElement);
        }
      );
    })
  }

  handleSubmit() {
    this.chipInstance.chipsData.forEach(chip => this.faKeyWords.push(new FormControl(chip.tag)));

    if (this.fg.valid) {
      this.$http
        .post<SubjectDTO>(`${environment.api_url}/subjects`, this.fg.value)
        .pipe(map(it => SubjectMapper.fromDTO(it)))
        .subscribe((data) => this.newSubjectEvent.emit(data));

      this.fg.reset({userId: 0});
      this.chipInstance.chipsData.splice(0, this.chipInstance.chipsData.length);
    }
  }
}
